import 'module-alias/register';

import 'mocha';

import { countRanges } from '#project/lodashByRange';
import { countRangesTesting } from './index.test';

countRangesTesting('LodashByRange version', countRanges);
