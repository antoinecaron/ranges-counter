import 'module-alias/register';

import { expect } from 'chai';
import 'mocha';
import { iterate } from '@plokkke/toolbox';
import _ from 'lodash';

import { RangeCount, RangeCounter } from '#project/index';

const VALUES: number[] = iterate(1000000).map(() => 100 * Math.random());
const RANGES: number[] = iterate(50).map(() => 100 * Math.random());
RANGES.sort((a, b) => a - b);

const counters: RangeCount[] = [];

describe('Speed tests', () => {
	it('Sort speed test', () => {
		[ ...VALUES ].sort((a, b) => a - b);
	});
	it('_.sort speed test', () => {
		_.sortBy([ ...VALUES ]);
	});
});

export function countRangesTesting(testingLabel: string, countRanges: RangeCounter): void {
	describe(testingLabel, () => {
		it('Sorted test', () => {
			const values: number[] = [ 1, 7 ];
			const ranges: number[] = [ 5 ];
			const result: RangeCount = { 0: 1, 1: 1 };
			return expect(countRanges(values, ranges)).deep.equal(result);
		});
		it('Not sorted test', () => {
			const values: number[] = [ 7, 1 ];
			const ranges: number[] = [ 5 ];
			const result: RangeCount = { 0: 1, 1: 1 };
			return expect(countRanges(values, ranges)).deep.equal(result);
		});
		it('Empty range test', () => {
			const values: number[] = [ 7, 1 ];
			const ranges: number[] = [ 2, 4, 5, 12 ];
			const result: RangeCount = { 0: 1, 1: 0, 2: 0, 3: 1, 4: 0 };
			return expect(countRanges(values, ranges)).deep.equal(result);
		});
		it('Floating test', () => {
			const values: number[] = [ 0.12, 12.3, 6.9, 2.3 ];
			const ranges: number[] = [ 5, 10 ];
			const result: RangeCount = { 0: 2, 1: 1, 2: 1 };
			return expect(countRanges(values, ranges)).deep.equal(result);
		});
		it('Speed test', () => {
			const rangeCount = countRanges([ ...VALUES ], RANGES);
			counters.push(rangeCount);
			expect(_.sum(Object.values(rangeCount))).equal(VALUES.length);
			counters.forEach((counter) => expect(counter).deep.equal(counters[0]));
		});
	});
}
