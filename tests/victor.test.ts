import 'module-alias/register';

import 'mocha';

import { countRanges } from '#project/victor';
import { countRangesTesting } from './index.test';

countRangesTesting('Victor version', countRanges);
