import 'module-alias/register';

import 'mocha';

import { countRanges } from '#project/vanilla';
import { countRangesTesting } from './index.test';

countRangesTesting('Vanilla version', countRanges);
