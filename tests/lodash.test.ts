import 'module-alias/register';

import 'mocha';

import { countRanges } from '#project/lodash';
import { countRangesTesting } from './index.test';

countRangesTesting('Lodash version', countRanges);
