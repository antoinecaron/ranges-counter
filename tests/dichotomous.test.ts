import 'module-alias/register';

import 'mocha';

import { countRanges } from '#project/dichotomous';
import { countRangesTesting } from './index.test';

countRangesTesting('Dichotomous version', countRanges);
