import { iterate } from '@plokkke/toolbox';
import _ from 'lodash';

import { RangeCount } from '#project/index';

type Bound = {
	index: number;
	rangeIndex?: number;
}

function countBySorted(array: number[], ranges: number[], counter: RangeCount, left: Bound, right: Bound): void {
	if (right.index < left.index) {
		return;
	}

	const pivot = Math.floor((right.index - left.index) / 2) + left.index;
	const rangeIndex = _.sortedIndex(ranges, array[pivot]);
	counter[rangeIndex] = (counter[rangeIndex] || 0) + 1;
	if (rangeIndex === left.rangeIndex) {
		counter[rangeIndex] += pivot - left.index;
	} else {
		countBySorted(array, ranges, counter, left, { index: pivot - 1, rangeIndex });
	}
	if (rangeIndex === right.rangeIndex) {
		counter[rangeIndex] += right.index - pivot;
	} else {
		countBySorted(array, ranges, counter, { index: pivot + 1, rangeIndex }, right);
	}
}

export function countRanges(values: number[], ranges: number[]): RangeCount {
	const rangeCount: RangeCount = {
		...iterate(ranges.length + 1).map(() => 0) as unknown as RangeCount,
	};

	values.sort((a, b) => a - b);
	countBySorted(values, ranges, rangeCount, { index: 0 }, { index: values.length - 1 });

	return rangeCount;
}
