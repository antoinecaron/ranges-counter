import 'module-alias/register';

export type RangeCount = Record<string, number>;
export type RangeCounter = (values: number[], ranges: number[]) => RangeCount;
