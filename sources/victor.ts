import { RangeCount } from '#project/index';

export function countRanges(values: number[], ranges: number[]): RangeCount {
	values.sort((a, b) => a - b);
	const counter: RangeCount = {};
	let rangeCount = 0;
	let counted = 0;

	for (let rangeIdx = 0; rangeIdx < ranges.length; rangeIdx += 1) {
		const sliced = values.slice(counted);
		rangeCount = sliced.findIndex((value) => value > ranges[rangeIdx]);
		if (rangeCount === -1) {
			rangeCount = sliced.length;
		}
		counter[rangeIdx] = rangeCount;
		counted += rangeCount;
	}
	counter[ranges.length] = values.length - counted;
	return counter;
}
