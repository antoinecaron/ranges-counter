import _ from 'lodash';
import { iterate } from '@plokkke/toolbox';

import { RangeCount } from '#project/index';

export function countRanges(values: number[], ranges: number[]): RangeCount {
	return {
		...iterate(ranges.length + 1).map(() => 0) as unknown as RangeCount,
		..._.countBy(values, (value) => _.sortedIndex(ranges, value)),
	};
}
