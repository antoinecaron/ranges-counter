import _ from 'lodash';

import { RangeCount } from '#project/index';


export function countRanges(values: number[], ranges: number[]): RangeCount {
	values.sort((a, b) => a - b);
	const indexes = _.map(ranges, (bound) => _.sortedIndex(values, bound));
	let counted = 0;
	const counter: RangeCount = _.transform(indexes, (counter: RangeCount, cummul: number, rangeCount: number) => {
		counter[rangeCount] = cummul - counted;
		counted = cummul;
	}, {});
	counter[ranges.length] = values.length - counted;
	return counter;
}
