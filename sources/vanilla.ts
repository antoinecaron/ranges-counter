import { iterate } from '@plokkke/toolbox';

import { RangeCount } from '#project/index';

export function findInsertIndex(array: number[], target: number, start = 0, end: number = array.length - 1): number {
	if (end < start) {
		return start;
	}

	const pivot = Math.floor((end - start) / 2) + start;
	const value = array[pivot];
	if (target < value) {
		return findInsertIndex(array, target, start, pivot - 1);
	}
	if (value < target) {
		return findInsertIndex(array, target, pivot + 1, end);
	}
	return pivot;
}

function countBy(array: number[], iteratee: (v: number) => number): RangeCount {
	return array.reduce((acc: RangeCount, value: number): RangeCount => {
		const type: number = iteratee(value);
		acc[type] = (acc[type] || 0) + 1;
		return acc;
	}, {});
}

export function countRanges(values: number[], ranges: number[]): RangeCount {
	return {
		...iterate(ranges.length + 1).map(() => 0) as unknown as RangeCount,
		...countBy(values, (value) => findInsertIndex(ranges, value)),
	};
}
